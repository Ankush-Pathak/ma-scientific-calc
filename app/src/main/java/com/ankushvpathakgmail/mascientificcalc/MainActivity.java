package com.ankushvpathakgmail.mascientificcalc;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    final int ADD = 1, SUB = 2, MUL = 3, DIV = 4, RES = 5, SIN = 6, TAN = 7, LOG = 8, FACT = 9;
    final String[] opString = {"NAO", "+", "-", "*", "/"};
    int op;
    long firstParam, secondParam, result;
    String input;
    boolean firstParamDone = false, secondParamDone;
    int operation;
    TextView textViewDisplay;
    int[] buttons = {R.id.buttonNum0, R.id.buttonNum1, R.id.buttonNum2, R.id.buttonNum3, R.id.buttonNum4
            , R.id.buttonNum5, R.id.buttonNum6, R.id.buttonNum6, R.id.buttonNum7, R.id.buttonNum8,
            R.id.buttonNum9, R.id.buttonAdd, R.id.buttonSub, R.id.buttonMul, R.id.buttonDiv, R.id.buttonResult, R.id.buttonCE, R.id.buttonSin, R.id.buttonTan, R.id.buttonFact, R.id.buttonLog};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textViewDisplay = findViewById(R.id.textViewDisplay);
        firstParamDone = false;
        secondParamDone = false;
        input = "";
        op = 0;
        for (int i = 0; i < buttons.length; i++) {
            findViewById(buttons[i]).setOnClickListener(MainActivity.this);
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonNum0:
                updateTextView("0");
                break;

            case R.id.buttonNum1:
                updateTextView("1");
                break;

            case R.id.buttonNum2:
                updateTextView("2");
                break;

            case R.id.buttonNum3:
                updateTextView("3");
                break;

            case R.id.buttonNum4:
                updateTextView("4");
                break;

            case R.id.buttonNum5:
                updateTextView("5");
                break;

            case R.id.buttonNum6:
                updateTextView("6");
                break;

            case R.id.buttonNum7:
                updateTextView("7");
                break;

            case R.id.buttonNum8:
                updateTextView("8");
                break;

            case R.id.buttonNum9:
                updateTextView("9");
                break;

            case R.id.buttonAdd:
                performOpHigh(ADD);
                break;

            case R.id.buttonSub:
                performOpHigh(SUB);
                break;

            case R.id.buttonMul:
                performOpHigh(MUL);
                break;

            case R.id.buttonDiv:
                performOpHigh(DIV);
                break;

            case R.id.buttonResult:
                performOpHigh(RES);
                break;

            case R.id.buttonCE:
                clear();
                break;

            case R.id.buttonSin:
                didSahaneFunction(SIN);
                break;

            case R.id.buttonTan:
                didSahaneFunction(TAN);
                break;

            case R.id.buttonLog:
                didSahaneFunction(LOG);
                break;

            case R.id.buttonFact:
                didSahaneFunction(FACT);
                break;
        }
    }

    void updateTextView(String digit) {
        input = input + digit;
        textViewDisplay.setText(input);
        Log.e("CALC", "updated input: " + input);
    }

    void performOpHigh(int operator) {
        if (operator != RES) {
            textViewDisplay.setText(opString[operator]);
            Log.e("CALC", "First if" + " input: " + input + " firstParamDone: " + firstParamDone + " firstParam: " + firstParam + " secondParam: " + secondParam
                    + " opertion: " + operation);
        } else if (operator == RES && firstParamDone) {
            secondParam = Integer.parseInt(input);
            textViewDisplay.setText(performOpLow(operation) + "");
            input = performOpLow(operation) + "";
            firstParamDone = false;
            Log.e("CALC", "Second else if" + " input: " + input + " firstParamDone: " + firstParamDone + " firstParam: " + firstParam + " secondParam: " + secondParam
                    + " opertion: " + operation);
            Log.e("CALC", "---------------------------------------");
        }

        if (!firstParamDone && operator != RES) {
            firstParam = Integer.parseInt(input);
            firstParamDone = true;
            operation = operator;
            input = "";
            Log.e("CALC", "Third else if" + " input: " + input + " firstParamDone: " + firstParamDone + " firstParam: " + firstParam + " secondParam: " + secondParam
                    + " opertion: " + operation);
        } else if (!(operator == RES)) {
            secondParam = Integer.parseInt(input);
            firstParam = performOpLow(operator);
            firstParamDone = true;
            input = "";
            operation = operator;
            Log.e("CALC", "Final else" + " input: " + input + " firstParamDone: " + firstParamDone + " firstParam: " + firstParam + " secondParam: " + secondParam
                    + " opertion: " + operation);
        }
        /*else if(!firstParamDone && secondParamDone)
        {
            result = performOpLow(operator);
            secondParamDone = false;
            firstParam = result;

            if(operator == RES)
            {
                textViewDisplay.setText(result + "");
            }
        }*/
    }

    void clear() {
        firstParamDone = false;
        textViewDisplay.setText("0");
        input = "";
    }

    long performOpLow(int operator) {
        switch (operator) {
            case ADD:
                return firstParam + secondParam;
            case SUB:
                return firstParam - secondParam;
            case MUL:
                return firstParam * secondParam;
            case DIV:
                return firstParam / secondParam;
            case SIN:
                return Math.round(Math.sin(firstParam));
            case TAN:
                return Math.round(Math.tan(firstParam));
            case FACT:
                long fact = 1;
                if (firstParam == 0 || firstParam == 1)
                    return 1;

                for (int i = 1; i <= firstParam; i++)
                    fact *= i;
                return fact;
            case LOG:
                return Math.round(Math.log(firstParam));
            case RES:
                return result;
        }
        return 0;
    }

    void didSahaneFunction(int operator) {
        firstParam = Integer.parseInt(input);
        firstParam = performOpLow(operator);
        input = firstParam + "";
        operation = operator;
        textViewDisplay.setText(input);
        Log.e("CALC", "Did sahane" + " input: " + input + " firstParamDone: " + firstParamDone + " firstParam: " + firstParam + " secondParam: " + secondParam
                + " opertion: " + operation);
    }

}
